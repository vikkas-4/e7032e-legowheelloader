# Code structure for the communication protocol
## Raspberry Pi (Python 3)
### Required packages:
	
* pyserial
* time?

### Structure: 

<figure>
<img src="pictures/Directory_structure.png" width="550">
<figcaption>Fig.x.x Directory structure<figcaption>
</figure>
### Predefined orders:
The orders are formatted as two bytes in succession.
The first byte refers to one of several predefined orders stated in the following table.


|Order|Reference(byte)|Reference(bits)|
|---|---|---|
|hello|0|0000|
|connected|1|0001|
|servo|2|0010|
|motor1 |3|0011|
|motor2 |4|0100|
|motor3 |5|0101|
|motor4 |6|0110|
|motor_arm |7|0111|
|motor_bucket |8|1000|

The following byte contains the reference value to the given actuator, 
since it's one byte the value will be in the range [0,255].




### Functions:

#### Handshake() 
// This function is meant to run one time on start up,
and to make sure a connection between the Raspberry Pi and the Arduino is established.

* Send handshake to Arduino.
* Wait for response.
* If response received, continue, else raise an error.


	
## Arduino (Arduino-C) or Nucleo (C)

### Functions:
#### begin.Serial()
// Inherent Arduino-C function

* Begin serial communication.
* set baud rate to 9600 (or 115200, needs to be the same value as on the Raspberry Pi!)
* the serial port is something like dev/ttyACM0 (check in the Arduino IDE under "Serial ports" or,
 by entering 'ls /dev/tty*' into the console.)

#### Serial.write()
// Inherent Arduino-C function,
[reference](https://www.arduino.cc/reference/en/language/functions/communication/serial/write/)

* Writes data to the serial port specified in `begin.Serial()`. 
* To write chars you must manually encode it to UTC-8 with `"a".encode('utc-8').`

#### Serial.parseInt()
// Inherent Arduino-C function,
[reference](https://www.arduino.cc/reference/en/language/functions/communication/serial/parseint/)

* Read an int from the serial port specified in `begin.Serial()`. 

#### CheckNewOrder()
// This function continuously checks for new orders on the serial line.
When a new order is received, it should make sure that the order is passed on to the correct actuator.
And also return to the RasPi that it was properly received.
	
* Wait for an order from the RasPi.
* Read and determine what kind of order(first byte?)
* Read the following value. (will an resolution of [0-255] a.k.a one byte be enough??)
* Send confirmation that the order was received back to the RasPi.
* Call the proper function for the corresponding order with the received value.

#### `void`  SetActuator(`int` act_value)
// One function for each actuator? When called sets a new value/PWM signal to the associated pin.

* Takes an integer "value" as input, no output.
* Write value to pin, probably through `analogWrite()`.








 