from orders import *
import serial

def handshake():
    """This function is meant to run one time on start up,
    and to make sure a connection between the Raspberry Pi and the Arduino is established.
    The RasPi sends the "hello" order to the MCU and waits for a response,
    the MCU is expected to return "connected" if it reads "hello" on the serial port."""

    while True:
        ser.write(hello)
        ser.write(hello)
        ser.write(hello)
        print("RasPi: " + str(hello) + " to MCU")# debug msg
        ser_byte = ser.read(1)# Read response
        print("MCU: " + str(ser_byte))
        if ser_byte == connected:# If MCU returns connected properly return, obs "connected" is a byte containing 0b01
            print("RasPi: " + str(ser_byte) + "ok! Return")# Debug msg
            return

"""
bool minus = false
int_16 sum = 0
uint_8 power = 0
read_until new line
byte
if byte == 0x2D {
minus = true
} else{
if byte between 0x30 and 0x39
	sum += pow(10, power) * (byte - 0x30)
	power += 1
if minus {
sum *= -1;

"""


