from orders import *
from functions import *

# Assign an binary representation (uint8) to each order and initially set the value to 0 for the actuators
hello = (0).to_bytes(1,byteorder='big', signed=False)
connected = (1).to_bytes(1,byteorder='big', signed=False)
servo = Order((2).to_bytes(1,byteorder='big', signed=False), 0)
motor_1 = Order((3).to_bytes(1,byteorder='big', signed=False), 0)
motor_2 = Order((4).to_bytes(1,byteorder='big', signed=False), 0)
motor_3 = Order((5).to_bytes(1,byteorder='big', signed=False), 0)
motor_4 = Order((6).to_bytes(1,byteorder='big', signed=False), 0)
motor_arm = Order((7).to_bytes(1,byteorder='big', signed=False), 0)
motor_bucket = Order((8).to_bytes(1,byteorder='big', signed=False), 0)
stop = (9).to_bytes(1,byteorder='big', signed=False)
received = (10).to_bytes(1,byteorder='big', signed=False)

# Open the serial port for communication and set the baudrate
ser = serial.Serial("/dev/ttyAMC0",9600)

# Make sure the input buffer is empty
ser.flushInput()

# Run the handshake function to make sure the RasPi and the MCU are communicating properly
# handshake()

# Set the servo to go straight
a = 100     # [0-255]
servo.value = a
servo.write()
print("Finished! :D")
ser.close()

