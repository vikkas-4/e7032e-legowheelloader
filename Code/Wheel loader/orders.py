class Order:
    def __init__ (self, order, value):
        """Predefined Order class that facilitate the communication between the RasPi and the MCU,
        the orders are defined as numbers between 0-8 and represented with the corresponding binary number.
        i.e. servo = 2 = b'/x10' """
        self.order = order
        self.value = value

    def write(self):

        sign = 0    # Positive values have sign = 0

        if self.value < 0:
            sign = 1    # If value is negative, set sign to 1

        msg = bytearray()   # Create a message to send to the MCU, this should be a bytearray = [order, sign, value] 
        msg.append(self.order)
        msg += bytes([sign, self.value])
        print(msg)  # Debugg msg
        ser.write(msg)  # Write the packaged message to the MCU

        print("wrote " + str(servo.value) + " to servo!")   # Debug msg

        ser_byte = ser.read(2)  # Read response
        
        if ser_byte[0] == 1:
            ser_byte[1] = -ser_byte[1]
        print("Read: " + str(ser_byte[1]))  # Debug msg
        return
       # if ser_byte == received:  # Make sure order was properly received before proceeding
        #    print("Order received!")
         #   return



