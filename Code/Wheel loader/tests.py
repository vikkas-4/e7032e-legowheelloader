from orders import *

# Initiate "order" as a "Order" class variable.
# Access the predefined orders with order.DESIRED_ORDER
order = Order()

def testorder(a):
    """ example function returning the input order.
    shows how the Order class can be used with if-statements"""
    if a == order.hello:
        print("Order 'hello' was receieved wtih reference: "  + str(a))
    elif a == order.connected:
        print("Order 'connected' was receieved wtih reference: "  + str(a))
    elif a == order.servo:
        print("Order 'servo' was receieved wtih reference: "  + str(a))
    elif a == order.motor_1:
        print("Order 'motor_1' was receieved wtih reference: "  + str(a))
    elif a == order.motor_2:
        print("Order 'motor_2' was receieved wtih reference: "  + str(a))
    elif a == order.motor_3:
        print("Order 'motor_3' was receieved wtih reference: "  + str(a))
    elif a == order.motor_4:
        print("Order 'motor_4' was receieved wtih reference: "  + str(a))
    elif a == order.motor_arm:
        print("Order 'motor_arm' was receieved wtih reference: "  + str(a))
    elif a == order.motor_bucket:
        print("Order 'motor_bucket' was receieved wtih reference: " + str(a))
    elif a == order.stop:
        print("Order 'stop' was receieved wtih reference: " + str(a))

def test_value(value):
    """example function receiving a value, one byte = [0,255],
     does some calculation and returns the value"""
    int_val = int(value,2)
    int_res = int_val/2

    print(str(int_val) + "/2 is " + str(int_res))
    return int_res

