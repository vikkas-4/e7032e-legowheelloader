// Number of pulses per revolution [PPR]
#define ENC_COUNT_REV 341

// Phase A and B IN from encoder
#define ENC_IN_A 3
#define ENC_IN_B 2

// ???
volatile long encoderValueA = 0;
volatile long encoderValueB = 0;

// Number of milliseconds until next calculation
int interval = 1000;

// Counters for milliseconds during interval
long previousMillis = 0;
long currentMillis = 0;

// Variable for RPM
int rpmA = 0;
int rpmB = 0;

String command = "0";

// ------------------------------------------------------------

void setup()
{
  // Setup serial
  Serial.begin(9600);

  // Setup encoder phase pins with a pullup resistor to minimize noise
  pinMode(ENC_IN_A, INPUT_PULLUP);
  pinMode(ENC_IN_B, INPUT_PULLUP);

  // Attach interrupt Counting pulses during interval
  attachInterrupt(digitalPinToInterrupt(ENC_IN_A), updateEncoderA, RISING);
  attachInterrupt(digitalPinToInterrupt(ENC_IN_B), updateEncoderB, RISING);

  // Setup initial value for timer
  previousMillis = millis();
}

// ------------------------------------------------------------

void loop()
{
  //
  if (Serial.available() > 0)
  {
    // Get incoming command
    command = Serial.readString();
  }

  if (command == "start")
  {
    Serial.print("Start");
    Serial.print("\t");
    Serial.println(command);
    command = '0';
  }
  else if (command == "stop")
  {
    Serial.print("Stop");
    Serial.print("\t");
    Serial.println(command);
    command = '0';
  }
  else
  {
  }

  // update RPM value every interval
  currentMillis = millis();
  if (currentMillis - previousMillis > interval)
  {
    previousMillis = currentMillis;

    // Calculate RPM
    rpmA = (float)(encoderValueA * 60 / ENC_COUNT_REV);
    rpmB = (float)(encoderValueB * 60 / ENC_COUNT_REV);

    // Update display
    Serial.print("Speed A:");
    Serial.print(rpmA);
    Serial.print("RPM");
    Serial.print("\t");
    Serial.print("Speed B:");
    Serial.print(rpmB);
    Serial.println("RPM");

    encoderValueA = 0;
    encoderValueB = 0;
  }
}

void updateEncoderA()
{
  // increment encoder value for each pulse from encoder
  encoderValueA++;
}

void updateEncoderB()
{
  // increment encoder value for each pulse from encoder
  encoderValueB++;
}
