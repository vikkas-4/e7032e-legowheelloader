<!-- {::options parse_block_html="true" /} -->

# Shopping list

## Actuators

* Motors with encoders
	* 4x motor: wheels
	* 1x motor: scoop
	* 1x motor: arm
* 2x servos for steering (if we choose to do it)

---

## Sensors
* 2x Current measurement sensor
* Motor encoders (Included inside the motors)

---

## Battery
* 11.1V Lihium-ion battery

---

## Computer/Microconrollers
* Arduino MEGA
* RaspberryPI

---

## Other circuits and electronics stuff
* Step down Converter (input voltage: 8-36V, output voltage: 1.25-32V)
* Power distribution board (for connectiong the battery with respective part in a clean way.)

---

## Misc
* super glue

---

## Tables of components and prices
The goods have boon searched in the following ordning according to the prioritization that was specified to us:  
1) ELFA  
2) RS components  
3) Farnell  
4) Digikey  
5) Mouser

---


---

##### Sold in ELFA:
| Component   |      Price   |  Quantity |  Link | Approved by Jan | Ordered by Andreas | Recieved by Andreas|
|----------|:-------------|:--|:------| :-- |:-------  | :-----   |
| Raspberry Pi 4 1GB| 355kr | 1 | [Elfa](https://www.elfa.se/sv/raspberry-pi-5ghz-quad-core-1gb-ram-raspberry-pi-pi4-model-1gb/p/30152779?track=true)| X |2019-09-27 |2019-10-04 |
| Arduino Mega | 331.96kr | 1 | [Elfa](https://www.elfa.se/sv/mikrostyrenhetskort-mega2560-r3-arduino-a000067/p/11038920?queryFromSuggest=true)| X |2019-09-27 | 2019-10-03|
| Super glue | 97.70kr  | 1 | [Elfa](https://www.elfa.se/en/instant-adhesive-loctite-loctite-454-3g-ch-de/p/11041259?track=true)| X  |2019-09-27 | 2019-10-03|
| Current Sensor | 98.5kr | 2 | [Elfa](https://www.elfa.se/en/ina219-high-side-dc-current-sensor-adafruit-904-sensor-breakout/p/30091225?track=true)| X  |2019-09-27 | 2019-10-03| 
<!-- | Charger | 653,00kr  | 1 |  [Elfa](https://elfa.se/en/charger-nimh-nicd-nimh-battery-pack-nicd-battery-pack-vanson-electronics-peak-80-bal/p/11094756?track=true) |   |   |   |  -->
---

##### Sold in RS components:
| Component   |      Price   |  Quantity |  Link | Approved by Jan | Ordered by Andreas | Recieved by Andreas| 
|----------|:-------------|:--|:------| :-- |:-------  | :-----   |
| Battery 11.1V | 768.26kr | 1  | [RS](https://se.rs-online.com/web/p/lithium-rechargeable-batteries/1445695/)| X | 2019-09-27  | 2019-10-03 |     

---

##### Sold in Digikey:
| Component   |      Price   |  Quantity |  Link | Approved by Jan | Ordered by Andreas | Recieved by Andreas|
|----------|:-------------|:--|:------| :-- |:-------  | :-----   |
| Motor controller | 48.02kr | 3 | [Digi-Key](https://www.digikey.se/product-detail/en/sparkfun-electronics/ROB-14451/1568-1756-ND/7915577?utm_adgroup=Boards+and+Kits&mkwid=s&pcrid=260422539758&pkw=&pmt=&pdv=c&productid=7915577&slid=&gclid=EAIaIQobChMIoPz8p-be5AIVTdOyCh2CrAihEAQYASABEgInn_D_BwE) | X |2019-09-27 |2019-10-03 | 
| Servo | 193.52kr | 2 | [Digi-Key](https://www.digikey.se/product-detail/sv/adafruit-industries-llc/1142/1528-1083-ND/5154658) | X |2019-09-27 |2019-10-03 | 

---

##### Sold in Mouser:
| Component   |      Price   |  Quantity |  Link | Approved by Jan | Ordered by Andreas | Recieved by Andreas|
|----------|:-------------|:--|:------| :-- |:-------  | :-----   |
| Motor | 191.82kr | 6 | [Mouser](https://www.mouser.se/ProductDetail/DFRobot/FIT0521?qs=0lQeLiL1qyZe5LlZGe9xQg==) | X |2019-09-27 |2019-10-03 |

---

##### Sold in Banggood but in none of the other 5 prioritized stores (ELFA, RS components, Farnell, Digikey, Mouser). 
| Component   |      Price   |  Quantity |  Link | Approved by Jan | Ordered by Andreas | Recieved by Andreas|
|----------|:-------------|:--|:------| :-- |:-------  | :-----   |
| Step down converter USB | 28.28kr | 1 | [Banggood](https://www.banggood.com/sv/LM2596S-Dual-USB-port-9V12V24V36V-to-5V-DC-DC-Step-Down-Buck-Car-Charger-Solar-3A-Power-Supply-Module-p-1417089.html?rmmds=search&cur_warehouse=CN) | X |2019-09-27 | |
| Step down converter | 9.72kr | 4 | [Banggood](https://www.banggood.com/sv/DC-DC-7-28V-to-5V-3A-Step-Down-Power-Supply-Module-Buck-Converter-Replace-LM2596-p-1536688.html?rmmds=search&cur_warehouse=CN) | X  |2019-09-27 | |
| Step down converter (5 pack) | 103.37kr | 1 | [Banggood](https://www.banggood.com/sv/5Pcs-XL4015-5A-DC-DC-Step-Down-Adjustable-Power-Supply-Module-Buck-Converter-p-1157549.html?rmmds=search-left-hotproducts__4&cur_warehouse=CN) | X  |2019-09-27 | |
| Power distribution board | 12.68kr | 2 | [Banggood](https://www.banggood.com/30x30-35x35-PCB-ESC-Power-Distribution-Board-For-MINI-Quadcopter-Multicopter-p-984686.html?rmmds=search&cur_warehouse=CN) | X  |2019-09-27 | |

---

##### ITprodukter:
Andreas sa att följande prudukter kan hittas bland våra itproduktwebb. Länkarna här finns bara för att beskriva produkterna.

| Component   |      Price   |  Quantity |  Link | Approved by Jan | Ordered by Andreas | Recieved by Andreas|
|----------|:-------------|:--|:------| :-- |:-------  | :-----   |
| microSD card | 201kr | 1 | [Elfa](https://www.elfa.se/sv/extreme-pro-microsd-memory-card-32-gb-sandisk-sdsqxaf-032g-gn6ma/p/30110938?track=true)|  X |2019-09-27 | |
| USB cable Type-C | 29.36kr | 1 | [Banggood](https://www.banggood.com/sv/BlitzWolf-BW-TC13-3A-USB-Type-C-Charging-Data-Cable-0_98ft0_3m-For-Oneplus-6-Xiaomi-Mi8-Mix-2s-S9-p-1339805.html?rmmds=search&ID=224&cur_warehouse=CN) | X |2019-09-27 | |



---

| C | P | Q | L | A | O | R |  <!--Component, Price, Quantitiy, Link, Site-name, Approval, Ordered, Recieved -->

--- 
