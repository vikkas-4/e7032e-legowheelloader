## Used pins on Nucleo 1 (Wheels)

| Pin number | Description | Usage | Comment |
|--------|:--------|:--|:--|
| CN7 |  |  |  |
| 1 | PC10 |  |  |
| 2 | PC11 |  |  |
| 3 | PC12 |  |  |
| 4 | PD2 |  |  |
| 5 | VDD, 3V3 |  |  |
| 6 | E5V |  |  |
| 7 | BOOT0 |  | Don't use |
| 8 | GND |  |  |
| 9 | NC |  |  |
| 10 | NC |  |  |
| 11 | NC |  |  |
| 12 | IOREF |  |  |
| 13 | PA13 |  | Don't use |
| 14 | RESET |  |  |
| 15 | PA14 |  | Don't use |
| 16 | 3V3 |  |  |
| 17 | PA15, PWM2/1 |  |  |
| 18 | 5V |  |  |
| 19 | GND |  |  |
| 20 | GND |  |  |
| 21 | PB7, PWM4/2 |  |  |
| 22 | GND |  |  |
| 23 | PC13 |  | User button |
| 24 | VIN |  |  |
| 25 | PC14 |  |  |
| 26 | NC |  |  |
| 27 | PC15 |  |  |
| 28 | PA0, PWM2/1, INT | Encoder FR Wheel |  |
| 29 | PH0 |  |  |
| 30 | PA1, PWM2/2, INT | Encoder RR Wheel|  |
| 31 | PH1 |  |  |
| 32 | PA4, INT | Encoder RL Wheel|  |
| 33 | VBAT |  |  |
| 34 | PB0 |  |  |
| 35 | PC2 |  |  |
| 36 | PC1 |  |  |
| 37 | PC3 |  |  |
| 38 | PC0 |  |  |
| CN10 |  |  |  |
| 1 | PC9, PWM3/4 |  |  |
| 2 | PC8, PWM3/3 |  |  |
| 3 | PB8, PWM4/3 | Motor controller, Mode, A/BIN1 | Wheel motors |
| 4 | PC6, PWM3/1 |  |  |
| 5 | PB9, PWM4/4 | Motor controller, Mode, A/BIN2 | Wheel motors |
| 6 | PC5 |  |  |
| 7 | AVDD |  |  |
| 8 | U5V |  | 5V from USB |
| 9 | GND |  |  |
| 10 | NC |  |  |
| 11 | PA5, PWM2/1, INT | Encoder FL Wheel|  |
| 12 | PA12 |  |  |
| 13 | PA6, PWM3/1, INT |  |  |
| 14 | PA11, PWM1/4 | PWMA Motor controller #1 | FR Wheel |
| 15 | PA7, PWM1/1N |  |  |
| 16 | PB12 |  |  |
| 17 | PB6, PWM4/1 |  |  |
| 18 | NC |  |  |
| 19 | PC7, PWM3/2 |  |  |
| 20 | GND |  |  |
| 21 | PA9, PWM1/2 | PWMB Motor controller #1 | FL Wheel |
| 22 | PB2 |  |  |
| 23 | PA8, PWM1/1, INT | PWMA Motor controller #2 | RR Wheel |
| 24 | PB1, PWM1/3N |  |  |
| 25 | PB10, PWM2/3 | Motor controller, Standby |  |
| 26 | PB15, PWM1/3N |  |  |
| 27 | PB4, PWM3/1 |  |  |
| 28 | PB14, PWM1/2N |  |  |
| 29 | PB5, PWM3/2 |  |  |
| 30 | PB13, PWM1/1N |  |  |
| 31 | PB3, PWM2/2 |  |  |
| 32 | AGND |  |  |
| 33 | PA10, PWM1/3, INT | PWMB Motor controller #2 | RL Wheel |
| 34 | PC4 |  |  |
| 35 | PA2, PWM2/3, INT | Used for USB comunication |  |
| 36 | NC |  |  |
| 37 | PA3, PWM2/4, INT | Used for USB comunication |  |
| 38 | NC |  |  |

*All odd pins on CN10 are digital 

## Used pins on Nucleo 2 (Bucket, arm, steering)

| Pin number | Description | Usage | Comment |
|--------|:--------|:--|:--|
| CN7 |  |  |  |
| 1 | PC10 |  |  |
| 2 | PC11 |  |  |
| 3 | PC12 |  |  |
| 4 | PD2 |  |  |
| 5 | VDD, 3V3 |  |  |
| 6 | E5V |  |  |
| 7 | BOOT0 |  | Don't use |
| 8 | GND |  |  |
| 9 | NC |  |  |
| 10 | NC |  |  |
| 11 | NC |  |  |
| 12 | IOREF |  |  |
| 13 | PA13 |  | Don't use |
| 14 | RESET |  |  |
| 15 | PA14 |  | Don't use |
| 16 | 3V3 |  |  |
| 17 | PA15, PWM2/1 |  |  |
| 18 | 5V |  |  |
| 19 | GND |  |  |
| 20 | GND |  |  |
| 21 | PB7, PWM4/2 |  |  |
| 22 | GND |  |  |
| 23 | PC13 |  | User button |
| 24 | VIN |  |  |
| 25 | PC14 |  |  |
| 26 | NC |  |  |
| 27 | PC15 |  |  |
| 28 | PA0, PWM2/1, INT | Encoder Arm A|  |
| 29 | PH0 |  |  |
| 30 | PA1, PWM2/2, INT | Encoder Arm B|  |
| 31 | PH1 |  |  |
| 32 | PA4, INT | Encoder Bucket A|  |
| 33 | VBAT |  |  |
| 34 | PB0 |  |  |
| 35 | PC2 |  |  |
| 36 | PC1 |  |  |
| 37 | PC3 |  |  |
| 38 | PC0 |  |  |
| CN10 |  |  |  |
| 1 | PC9, PWM3/4 |  |  |
| 2 | PC8, PWM3/3 |  |  |
| 3 | PB8, PWM4/3 | Motor controller, Mode, AIN1 | Bucket motor |
| 4 | PC6, PWM3/1 | PWM Servo |  |
| 5 | PB9, PWM4/4 | Motor controller, Mode, AIN2 | Bucket motor |
| 6 | PC5 |  |  |
| 7 | AVDD |  |  |
| 8 | U5V |  | 5V from USB |
| 9 | GND |  |  |
| 10 | NC |  |  |
| 11 | PA5, PWM2/1, INT | Encoder Bucket B|  |
| 12 | PA12 |  |  |
| 13 | PA6, PWM3/1, INT |  |  |
| 14 | PA11, PWM1/4 | PWMA Motor controller | Bucket Motor |
| 15 | PA7, PWM1/1N |  |  |
| 16 | PB12 |  |  |
| 17 | PB6, PWM4/1 | Motor controller, Mode, BIN1 | Arm motor |
| 18 | NC |  |  |
| 19 | PC7, PWM3/2 | Motor controller, Mode, BIN2 | Arm motor |
| 20 | GND |  |  |
| 21 | PA9, PWM1/2 |  |  |
| 22 | PB2 |  |  |
| 23 | PA8, PWM1/1, INT |  |  |
| 24 | PB1, PWM1/3N |  |  |
| 25 | PB10, PWM2/3 | Motor controller, Standby |  |
| 26 | PB15, PWM1/3N |  |  |
| 27 | PB4, PWM3/1 |  |  |
| 28 | PB14, PWM1/2N |  |  |
| 29 | PB5, PWM3/2 |  |  |
| 30 | PB13, PWM1/1N |  |  |
| 31 | PB3, PWM2/2 |  |  |
| 32 | AGND |  |  |
| 33 | PA10, PWM1/3, INT | PWMB Motor controller | Arm motor |
| 34 | PC4 |  |  |
| 35 | PA2, PWM2/3, INT | Used for USB comunication |  |
| 36 | NC |  |  |
| 37 | PA3, PWM2/4, INT | Used for USB comunication |  |
| 38 | NC |  |  |

*All odd pins on CN10 are digital 

## Used pins on Raspberry

| Pin number | Description | Usage |
|--------|:--------|:--|
| 1 | 3v3 |  |
| 2 | 5v |  |
| 3 | GPIO 2, I2C(SDA1), I2C(SDA3), |  |
| 4 | 5v |  |
| 5 | GPIO 3, I2C(SCL3), |  |
| 6 | GND |  |
| 7 | GPIO 4, I2C(SDA3), |  |
| 8 | GPIO 14|  |
| 9 | GND |  |
| 10 | GPIO 15|  |
| 11 | GPIO 17|  |
| 12 | GPIO 18|  |
| 13 | GPIO 27|  |
| 14 | GND |  |
| 15 | GPIO 22, I2C(SDA6), |  |
| 16 | GPIO 23, I2C(SCL6), |  |
| 17 | 3v3 |  |
| 18 | GPIO 24|  |
| 19 | GPIO 10, I2C(SDA5), |  |
| 20 | GND |  |
| 21 | GPIO 9, I2C(SCL4), |  |
| 22 | GPIO 25|  |
| 23 | GPIO 11, I2C(SCL5)|  |
| 24 | GPIO 8, I2C(SDA4), |  |
| 25 | GND |  |
| 26 | GPIO 7, I2C(SCL4), |  |
| 27 | GPIO 0, I2C(SDA0), | Current sensor |
| 28 | GPIO 1, I2C(SCL0)| Current sensor |
| 29 | GPIO 5, I2C(SCL3), |  |
| 30 | GND |  |
| 31 | GPIO 6, I2C(SDA4), |  |
| 32 | GPIO 12, I2C(SDA5),|  |
| 33 | GPIO 13, I2C(SCL5), |  |
| 34 | GND |  |
| 35 | GPIO 19|  |
| 36 | GPIO 16|  |
| 37 | GPIO 26|  |
| 38 | GPIO 20|  |
| 39 | GND |  |
| 40 | GPIO 21|  |


## Pins needed for actuators and sensors 

| Component | Pin type | # of pins | Allocated |
|--------|:--------|:--|:--|
| Encoder | Interrupt | 8 | X |
| Motor controller | PWM | 6 | X |
| Motor controller, Standby | Digital | 2 (3?) | X |
| Motor controller, Mode | Digital | 6 (12?) | X |
| Servo | PWM | 1 | X |
| Current sensor | I2C | 2 | X |




## Used pins on Arduino

| Pin number | Description | Usage | Comment |
|--------|:--------|:--|:--|
| A0 | Analog |  |  |
| A1 | Analog |  |  | 
| A2 | Analog |  |  | 
| A3 | Analog |  |  | 
| A4 | Analog |  |  | 
| A5 | Analog |  |  | 
| A6 | Analog |  |  | 
| A7 | Analog |  |  | 
| A8 | Analog |  |  | 
| A9 | Analog |  |  | 
| A10 | Analog |  |  | 
| A11 | Analog |  |  | 
| A12 | Analog |  |  | 
| A13 | Analog |  |  | 
| A14 | Analog |  |  | 
| A15 | Analog |  |  |
| 0 | RX, USB to Serial |  |  |
| 1 | TX, USB to Serial |  |  | 
| 2 | PWM, INT0 |  |   |
| 3 | PWM, INT1 |  |   |
| 4 | PWM | Motor Controller #1 A| For bucket motor | 
| 5 | PWM | Motor Controller #2 A| For arm motor | 
| 6 | PWM | Motor Controller #1 B| For Front Right wheel motor | 
| 7 | PWM | Motor Controller #2 B| For Rear Right wheel motor | 
| 8 | PWM | Motor Controller #3 A| For Rear Left wheel motor | 
| 9 | PWM | Motor Controller #3 B| For Front Left wheel motor | 
| 10 | PWM |  |   |
| 11 | PWM |  |   |
| 12 | PWM |  |   |
| 13 | PWM, LED |  |   |
| 14 | TX |  |   |
| 15 | RX |  |   |
| 16 | TX |  |   |
| 17 | RX |  |   |
| 18 | TX, INT5 |  |   |
| 19 | RX, INT4 |  |   |
| 20 | I2C(SDA), INT3 |  |  | 
| 21 | I2C(SCL), INT2 |  |  | 
| 22 |  | Motor controller #1 AI1, Mode| For bucket motor | 
| 23 |  | Motor controller #1 AI2, Mode| For bucket motor | 
| 24 |  | Motor controller #2 AI1, Mode| For arm motor | 
| 25 |  | Motor controller #2 AI2, Mode| For arm motor | 
| 26 |  | Motor controller, Mode | For Wheels | 
| 27 |  | Motor controller, Mode | For Wheels | 
| 28 |  | (Motor controller, Mode) |  | 
| 29 |  | (Motor controller, Mode) |  | 
| 30 |  | (Motor controller, Mode) |  | 
| 31 |  | (Motor controller, Mode) |  | 
| 32 |  | (Motor controller, Mode) |  | 
| 33 |  | (Motor controller, Mode) |   |
| 34 |  |  |   |
| 35 |  |  |   |
| 36 |  | (Motor controller #1, Standby) |   |
| 37 |  |  |   |
| 38 |  | Motor controller (#1) #2 (#3), Standby |   |
| 39 |  |  |   |
| 40 |  | (Motor controller #3, Standby) |  | 
| 41 |  |  |   |
| 42 |  |  |   |
| 43 |  |  |   |
| 44 | PWM | Servo |   |
| 45 | PWM | (Servo) |   |
| 46 | PWM |  |   |
| 47 |  |  |   |
| 48 |  |  |   |
| 49 |  |  |   |
| 50 | SPI(MISO) |  |   |
| 51 | SPI(MOSI) |  |   |
| 52 | SPI(SCK) |  |   |
| 53 | SPI(SS) |  |   |
| 54 |  |  |   |