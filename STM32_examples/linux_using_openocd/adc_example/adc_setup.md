# ADC Dual Channel Example

## Useful links

[Joystick]([https://wiki.dfrobot.com/Joystick_Module_For_Arduino_SKU_DFR0061](https://wiki.dfrobot.com/Joystick_Module_For_Arduino_SKU_DFR0061)

[Youtube Tutorial]([https://www.youtube.com/watch?v=6U1uyEjoPu0&list=PLfExI9i0v1sn-GXGKpH1Rgjta04-30Z6u&index=4&t=65s](https://www.youtube.com/watch?v=6U1uyEjoPu0&list=PLfExI9i0v1sn-GXGKpH1Rgjta04-30Z6u&index=4&t=65s)

## Pinout Configuration

First we need to configure the pinout of the microcontroller. PC13 adn PA5 are configured for push button and LED but no needed for this example.

PA0 and PA1 is configured as Channel 0 and 1 of the ADC1 as can be seen below in the pinout:

![](pictures/adc_pinConfig.png)

Then the settings of the ADC1 is configured with a resolution of 8bits (11 ADC Clock Cycles) and with a samplingtime of 28 Cycles as shown below:

![](pictures/adc_settings.png)

## Hardware

The Joystick used has three I/Os, GND (Ground), VCC (Power) and S (signal). This is shown in the picture below. The Z-axis I/O is not used.

![](pictures/joystick.png)

The Joystick is connected to the STM32F401RE using the marked (in red) pins. (these pin was just the easiest to use at the time) This is shown in the picture below:

![](pictures/joystick_pinConnection.png)

Since pins PA0 and PA1 are the PWM inputs, these can be switched to another pwm source to test the code if you do not have a joystick availible.
