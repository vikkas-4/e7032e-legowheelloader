# PWM Example

## CubeMX
Configure PC13 to be a GPIO_INPUT which is connected to the onboard button.

Setup timer 3 (TIM3) with Clock source "internal clock" and channel 1 as ""PWM generation CH1".  

Configure the Prescaler(PSC) of 84 and Counter period(ARR) of 100. this results in a 10kHz pwm signal generation accoring to the calculation  
"CLK/((ARR)*(PSC))"


## Connections
Probe the PA6 and ground using an oscilloscope to see the output pwm signal changing.
Should look something like this:

![](pictures/pwm.jpg)