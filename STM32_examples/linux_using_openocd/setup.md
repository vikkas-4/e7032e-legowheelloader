# STM32F401RE NUCLEO Development Board

[Youtube Playlist](https://www.youtube.com/playlist?list=PLfExI9i0v1sn-GXGKpH1Rgjta04-30Z6u)

## CubeMX HowTo
1. Open CubeMX
2. Click "Start My project from MCU"
3. Choose STM32F401RE as the board
4. Clear pinout by clicking Pinout/"Clear Pinouts"
5. Change pinout as desired
6. Change the clock configuration as desired
7. Go to Project Manager
8. Save the project with the desired name
9. Change the Toolchain/IDE as desired
10. Generate Code!

## Coding
1. Open the src/"main.c" file inside the project folder
2. Write Code!   
IMPORTANT: only write code between "USER CODE BEGIN" and ""USER CODE END". If changes are made in CubeMX, all code outside those fields will be terminated.
3. Compile the program.



## Openocd and gdb debugging
Move the gdb.gdb configuration file to the project folder where the "Makefile" is.

Terminal 1: Openocd is used as a interface to talk to the board. Run the following command while the board is connected to start the interface:
  
$ sudo openocd -f openocd.cfg   

Terminal 2: gdb is used for debugging. Inside project folder, run these commands for each code update:  
1. $ make  
2. $ gdb-multiarch build/project_name.elf -x gdb.gdb 


## Reading Serial using putty
1. Start Putty
2. Choose "serial"
3. Choose the correct port(Serial line) to monitor
4. define the correct baudrate(Speed)
5. Open the serial by clicking "open"
