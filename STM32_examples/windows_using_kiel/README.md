## Software (Debugging in Kiel)

Build the project (F7) then load it to the board (F8) when everything is connected int the correct way. Then click the debug button (ctrl + F5) in the top right as shown below:

![](C:\Users\Vikke\Projects\e7032e-legowheelloader\STM32_examples\windows_using_kiel\adc_example\pictures\debugging.png)

To see the data of a variable, right click it in the code and you should see the following option to add the variable to Watch 1 as shown below:

![](C:\Users\Vikke\Projects\e7032e-legowheelloader\STM32_examples\windows_using_kiel\adc_example\pictures\watch_variable.png)

Then the selected variable should pop up in the watch 1 Window in the bottom of the program as shown here:

![](C:\Users\Vikke\Projects\e7032e-legowheelloader\STM32_examples\windows_using_kiel\adc_example\pictures\watch_1.png)

Then run the program in debugging mode using the "Run" button (F5) in the top left as shown below:

![](C:\Users\Vikke\Projects\e7032e-legowheelloader\STM32_examples\windows_using_kiel\adc_example\pictures\run.png)


