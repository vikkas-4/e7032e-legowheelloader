# STM32F401RE Setup and HowTo - Windows

## Installing

Install the following two programs to be able to change the pinout of the microcontroller and also Have a IDE to work with.

CubeMX(you need to create an account): [Link](https://www.st.com/en/development-tools/stm32cubemx.html)

Kiel: [Link](https://www.keil.com/demo/eval/arm.htm)

## CubeMX

1. Choose the correct board
2. Start working! [Tutorial](https://www.youtube.com/playlist?list=PLfExI9i0v1sn-GXGKpH1Rgjta04-30Z6u) The tutorial examples are under [STM32_examples](STM32_examples)
3. Toolchain/IDE under project Manager should be selected as ""MDK-ARM"
4. Generate the code
5. Open project in Kiel/microvision

## Kiel

1. Open project
2. Build the project (F7)
3. Download to the board!

If error message "st-link not detected" then do the following  

    1. Go to where you installed Kiel and go to USBDriver, in my case "C:\Keil_v5\ARM\STLink\USBDriver"  
    2. Install the file "dpinst_amd64.exe"  
    3. Finished!  

Go to "Options for target" in the menu and the "Utilities" uncheck the "Use Debug Driver" and then click settings and check that under "Debug" it says "Unit: ST-LINK/V2-1"

Then reload the program and try to download to the board again and it should work!
