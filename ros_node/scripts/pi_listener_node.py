#!/usr/bin/env python
# Import necessery packages
import rospy            # Package for using ROS with python
import struct           # Package for packing and unpacking binary data
import serial           # Package for sending and receiving data via the s$
from ros_node.msg import order_msg      # Import the custom message type d$


class Order:
    def __init__ (self, order, sign, value):
        """Predefined Order class that facilitate the communication between the RasPi and the MCU,
        the orders are defined as numbers between 0-12 and represented with the corresponding binary number.
        i.e. servo = 2 = b'/x10' """
        self.order = order
        self.sign = sign
        self.value = value

    def write(self):
	""" This function writes a Order class object to the MCU.
	    standard procedure to write a message is:
		1) Update the objects fields with the decired values
			self.order = ...
			self.sign = ...
			self.value = ...
		2) Call  "self.write()"
	"""

        msg = bytearray()   		# Create a message to send to the MCU, this should be a bytearray = [order, sign, value] 
        # Add the values to the bytearray
        msg.append(self.order)
	msg.append(self.sign)
	msg.append(self.value)

#	print struct.unpack("BBB", msg)
	if self.order == 4 or self.order == 5:
		ser_arm.write(msg)  		# Write the packaged message to the MCU
#		print "wrote to arm/bucket"
	else:
		ser_drive.write(msg)
#		print "wrote to drive"
	return

def handshake():
	""" The handshake() function makes sure that the connection between the RasPI and the MCU is working before proceeding """

	msg_startup = bytearray()	# The MCU always reads 3 bytes so it is necessary to send an array with 3 bytes
	msg_startup.append(hello)	# For simplicity we just repeat the same order 3 times
	msg_startup.append(hello)
	msg_startup.append(hello)
	ser_drive.write(msg_startup)
	ser_arm.write(msg_startup)

	while True:			# Wait until "connection" is recevied form the MCU
		response_drive = ser_drive.read(1)	# Read the response from MCU1
		response_arm = ser_arm.read(1)
		if response_arm == connected and response_arm == connected:
			print("Connection establised!")
			return
		else:
			print("MCU not connected")

def callback(data):
	""" This function is called whenever there is the listener() receives a new message.
	    It first identifies which actuator the AI want to write to and then updates the values and writes to the MCU. """

#	rospy.loginfo(rospy.get_caller_id() + "Actuator: %s ; Sign: %s ; Value: %s", data.order, data.sign, data.value)		# Debugg message, uncomment to write the received message to the terminal.

	if data.order == 2:			# servo
        	servo.value = data.value
        	servo.sign = data.sign
        	servo.write()
		return

	elif data.order == 3:			# drive
		drive.value = data.value
		drive.sign = data.sign
		drive.write()
		return

        elif data.order == 4:			# motor_arm
                motor_arm.value = data.value
                motor_arm.sign = data.sign
                motor_arm.write()
                return

        elif data.order == 5:			# motor_bucket
                motor_bucket.value = data.value
                motor_bucket.sign = data.sign
                motor_bucket.write()
                return

        elif data.order == 10:			#stop. Stop sends a 3 byte long bytearray with "stop" 
                stop_msg = bytearray()
		stop_msg.append(stop)
		stop_msg.append(stop)
		stop_msg.append(stop)
		ser_arm.write(stop_msg)
        	ser_bucket.write(stop_msg)
	        return
	else:
		print("error: undefined order")
		return

def listener():

        rospy.init_node('pi_listener', anonymous=True)		# Initiate the listener node
	rospy.Subscriber("order_topic", order_msg, callback)	# Set the node to subscribe to the 'order_topic'
        rospy.spin()						# spin() simply keeps python from exiting until this node is stopped

if __name__ == '__main__':

	# Assign a binary representation (uint8) to each order and initiate sign and value as 0.
	hello = struct.pack("B", 0)
	connected = struct.pack("B", 1)
	servo = Order(2, 0, 0)
	drive = Order(3, 0, 0)
	motor_arm = Order(4, 0, 0)
	motor_bucket = Order(5, 0, 0)
	stop = struct.pack("B", 10)

	########## Start communication #####################

	ser_drive = serial.Serial("/dev/ttyACM1",115200)		# Open the serial port for communication and set the baudrate
	ser_arm = serial.Serial("/dev/ttyACM0",115200)
	ser_drive.flushInput()					# Make sure the input buffer is empty
	ser_arm.flushInput()                                    # Make sure the input buffer is empty

	handshake() 						# Run the handshake function to make sure the RasPi and the MCU connected

	listener()						# Start the listener
	ser_drive.close()						# When finished close the serial port
	ser_arm.close()
