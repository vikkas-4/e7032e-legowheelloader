#!/usr/bin/env python
# Import necessary packages
import rospy		# Package for using ROS with python
import struct		# Package for packing and unpacking binary data
import serial		# Package for sending and receiving data via the serial port
from ros_node.msg import order_msg	# Import the custom message type defined in ros_msg.msg


def talker():
	""" This function reads sensor data from the MCU and publishes it
	to the 'data_topic' topic."""

#        rospy.loginfo(order_msg)        # This function writes the published message to the terminal, uncomment this line for debugging.

        pub = rospy.Publisher('data_topic', order_msg, queue_size=10)			# Setup the publisher to write 'order_msg' to 'data_topic'.
        rospy.init_node('pi_talker', anonymous=True)					# Start the node
        while not rospy.is_shutdown():							# Keeps the program running as long as the node is up, use CTRL+C to terminate.        	                
		arm_pos = ser_arm.read(3)
		bucket_pos = ser_arm.read(3)
		arm_speed = ser_arm.read(3)
		bucket_speed = ser_arm.read(3)
		drive_data = ser_drive.read(3)						# Read the serial port. Expects 3 bytes in the following order [order, sign, value]

		print struct.unpack("BBB", arm_pos )
		print struct.unpack("BBB", bucket_pos )
		print struct.unpack("BBB", arm_speed )
		print struct.unpack("BBB", bucket_speed )
		print struct.unpack("BBB", drive_data )



		if len(drive_data) == 3:						# Make sure the full message was read
			drive_data = struct.unpack("BBB", drive_data)			# Converts the binary data back to numbers for examble: 0b10 --> 2
			if drive_data[0] == 3:						# The actuators that give sensor data
				pub.publish(drive_data[0], drive_data[1], drive_data[2])	# Send the sensor data back to the AI
			else:
				print("\nerror: Drive data not correct.\n")
		else:
			print("\nser_drive.read() exited without reading the required amount of bytes.\n")	# Exit if the timeout specified in serial.Serial(..., timeout=...) is exceded. 

		if len(arm_pos) == 3:
                        arm_pos = struct.unpack("BBB", arm_pos )
                        if arm_pos[0] == 6:
                                pub.publish(arm_pos[0], arm_pos[1], arm_pos[2])
                        else:
                                print("\nerror: arm_pos data not correct.\n")
                else:
                        print("\nser_arm.read() exited without reading.\n")


		if len(arm_speed) == 3:
                        arm_speed = struct.unpack("BBB", arm_speed)
                        if arm_speed[0] == 4:
                                pub.publish(arm_speed[0], arm_speed[1], arm_speed[2])
                        else:
                                print("\nerror: arm_speed data not correct.\n")
                else:
                        print("\nser_arm.read() exited without reading.\n")

                if len(bucket_pos) == 3:
                        bucket_pos = struct.unpack("BBB", bucket_pos )
                        if bucket_pos[0] == 7:
                                pub.publish(bucket_pos[0], bucket_pos[1], bucket_pos[2])
                        else:
                                print("\nerror: bucket_pos data not correct.\n")
                else:
                        print("\nser_arm.read() exited without reading.\n")

                if len(bucket_speed) == 3:
                        bucket_speed = struct.unpack("BBB", bucket_speed)
                        if bucket_speed[0] == 5:
                                pub.publish(bucket_speed[0], bucket_speed[1], bucket_speed[2])
                        else:
                                print("\nerror: bucket_speed data not correct.\n")
                else:
                        print("\nser_arm.read() exited without reading.\n")



if __name__ == '__main__':
	try:
		ser_drive = serial.Serial("/dev/ttyACM1",115200, timeout = 30)          # The program doesn't shutdown propery if a timeout is not specified.
	        ser_arm = serial.Serial("/dev/ttyACM0",115200, timeout = 30)
		talker()

        except rospy.ROSInterruptException:
                pass

