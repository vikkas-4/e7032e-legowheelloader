#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/ros/wheelloaderROS/devel/.private/aidriver:$CMAKE_PREFIX_PATH"
export PWD="/home/ros/wheelloaderROS/build/aidriver"
export ROSLISP_PACKAGE_DIRECTORIES="/home/ros/wheelloaderROS/devel/.private/aidriver/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/ros/wheelloaderROS/src/aidriver:$ROS_PACKAGE_PATH"