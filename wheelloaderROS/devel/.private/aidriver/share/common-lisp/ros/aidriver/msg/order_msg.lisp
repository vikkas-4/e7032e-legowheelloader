; Auto-generated. Do not edit!


(cl:in-package aidriver-msg)


;//! \htmlinclude order_msg.msg.html

(cl:defclass <order_msg> (roslisp-msg-protocol:ros-message)
  ((order
    :reader order
    :initarg :order
    :type cl:fixnum
    :initform 0)
   (sign
    :reader sign
    :initarg :sign
    :type cl:fixnum
    :initform 0)
   (value
    :reader value
    :initarg :value
    :type cl:fixnum
    :initform 0))
)

(cl:defclass order_msg (<order_msg>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <order_msg>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'order_msg)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name aidriver-msg:<order_msg> is deprecated: use aidriver-msg:order_msg instead.")))

(cl:ensure-generic-function 'order-val :lambda-list '(m))
(cl:defmethod order-val ((m <order_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader aidriver-msg:order-val is deprecated.  Use aidriver-msg:order instead.")
  (order m))

(cl:ensure-generic-function 'sign-val :lambda-list '(m))
(cl:defmethod sign-val ((m <order_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader aidriver-msg:sign-val is deprecated.  Use aidriver-msg:sign instead.")
  (sign m))

(cl:ensure-generic-function 'value-val :lambda-list '(m))
(cl:defmethod value-val ((m <order_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader aidriver-msg:value-val is deprecated.  Use aidriver-msg:value instead.")
  (value m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <order_msg>) ostream)
  "Serializes a message object of type '<order_msg>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'order)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'sign)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'value)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <order_msg>) istream)
  "Deserializes a message object of type '<order_msg>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'order)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'sign)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'value)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<order_msg>)))
  "Returns string type for a message object of type '<order_msg>"
  "aidriver/order_msg")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'order_msg)))
  "Returns string type for a message object of type 'order_msg"
  "aidriver/order_msg")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<order_msg>)))
  "Returns md5sum for a message object of type '<order_msg>"
  "f4db8f975c630eb8d4b6818527d8ebdc")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'order_msg)))
  "Returns md5sum for a message object of type 'order_msg"
  "f4db8f975c630eb8d4b6818527d8ebdc")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<order_msg>)))
  "Returns full string definition for message of type '<order_msg>"
  (cl:format cl:nil "uint8 order~%uint8 sign~%uint8 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'order_msg)))
  "Returns full string definition for message of type 'order_msg"
  (cl:format cl:nil "uint8 order~%uint8 sign~%uint8 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <order_msg>))
  (cl:+ 0
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <order_msg>))
  "Converts a ROS message object to a list"
  (cl:list 'order_msg
    (cl:cons ':order (order msg))
    (cl:cons ':sign (sign msg))
    (cl:cons ':value (value msg))
))
