;; Auto-generated. Do not edit!


(when (boundp 'aidriver::order_msg)
  (if (not (find-package "AIDRIVER"))
    (make-package "AIDRIVER"))
  (shadow 'order_msg (find-package "AIDRIVER")))
(unless (find-package "AIDRIVER::ORDER_MSG")
  (make-package "AIDRIVER::ORDER_MSG"))

(in-package "ROS")
;;//! \htmlinclude order_msg.msg.html


(defclass aidriver::order_msg
  :super ros::object
  :slots (_order _sign _value ))

(defmethod aidriver::order_msg
  (:init
   (&key
    ((:order __order) 0)
    ((:sign __sign) 0)
    ((:value __value) 0)
    )
   (send-super :init)
   (setq _order (round __order))
   (setq _sign (round __sign))
   (setq _value (round __value))
   self)
  (:order
   (&optional __order)
   (if __order (setq _order __order)) _order)
  (:sign
   (&optional __sign)
   (if __sign (setq _sign __sign)) _sign)
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:serialization-length
   ()
   (+
    ;; uint8 _order
    1
    ;; uint8 _sign
    1
    ;; uint8 _value
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _order
       (write-byte _order s)
     ;; uint8 _sign
       (write-byte _sign s)
     ;; uint8 _value
       (write-byte _value s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _order
     (setq _order (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _sign
     (setq _sign (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _value
     (setq _value (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;;
   self)
  )

(setf (get aidriver::order_msg :md5sum-) "f4db8f975c630eb8d4b6818527d8ebdc")
(setf (get aidriver::order_msg :datatype-) "aidriver/order_msg")
(setf (get aidriver::order_msg :definition-)
      "uint8 order
uint8 sign
uint8 value

")



(provide :aidriver/order_msg "f4db8f975c630eb8d4b6818527d8ebdc")


