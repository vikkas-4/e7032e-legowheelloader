#!/usr/bin/env python
# Import necessary packages
import rospy				# Package for using ROS with python
from aidriver.msg import order_msg	# Import the custom message type defined in order_msg.msg

def callback(data):
	rospy.loginfo("Actuator: %s ; Value: %s", data.order, data.value)


def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.

    rospy.init_node('ai_listener', anonymous=True)
    rospy.Subscriber("data_topic", order_msg, callback)
    rospy.spin()	# spin() simply keeps python from exiting until this node is stopped


if __name__ == '__main__':
    listener()

