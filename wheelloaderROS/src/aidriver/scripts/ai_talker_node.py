#!/usr/bin/env python
import rospy
from aidriver.msg import order_msg
import pygame

pygame.init()
pygame.joystick.init()
joystick = pygame.joystick.Joystick(0)
joystick.init()

""" This is the AI-talker node. It takes inputs from the user/AI
via the pygames package and an xbox controller and publishes the
reference values to the order_topic topic."""

class Order:
    def __init__ (self, order, sign, value):
        """Predefined Order class that facilitate the communication betwee$
        the orders are defined as numbers between 0-12 and represented wit$
        i.e. servo = 2 = b'/x10' """
        self.order = order
        self.sign = sign
        self.value = value


# Takes 3 values as input and publishes them as a 'order_msg' message on the 'order_topic' topic
def talker():
	rospy.loginfo(order_msg)	# This function writes the published message to the terminal, uncomment this line for debugging.
        pub = rospy.Publisher('order_topic', order_msg, queue_size=10)	# Setup the publisher to write 'order_msg' to 'order_topic'. queue_size sets how many msgs can be queued at one time.
        rospy.init_node('ai_talker', anonymous=True)	# Start the ROS node
	r = rospy.Rate(20)
        while not rospy.is_shutdown():    # Keeps the program running for as long as the node is up, use CTRL+C to terminate.

	       	####################################
                #   Reference values from the "AI" #
                ####################################
		""" To publish a message do the following procedure:
			1) Update the vaiables X.sign and X.value with the desired values, see below for an explanation of what these should be.
			2) Publish to the ROS topic with "pub.publish(X.order, X.sign, X.value)".

		    The messages published must be of type 'order_msg' i.e., have the following structure:
			----------------
			uint8	order
			uint8	sign
			uint8	value
			----------------

		   * drive:
			To drive the wheelloader use this message:

			-----------
			drive.order
			drive.sign	0 = forward, 1 = backward
			drive.value	value of the speed in the range [0,255] where 0 is has the wheelloader standing still, and 255 is max speed.
			-----------

		   * stearing:
			To stear the wheelloader use this message:

			-----------
			servo.order
			servo.sign	1 = negative = left, 0 = positive = right
			servo.value	how much the servo should turn, 0 is straight and 255 is max. (obs, the direction is set by the sign bit.)
			-----------

		   * motor arm & bucket:
			To controll the arm and bucket of the wheelloader use this message:

			--------------
			motor_**.order	OBS! ** is either "arm" or "bucket"
			motor_**.sign	the sign bit sets the direction of the arm/bucket, 0 = up, 1 = down
			motor_**.value	value of the speed in the range [0,255] where 0 has the arm/bucket still and 255 is max speed
			--------------

		   * stop:
			To stop the wheelloader use this message:

			The MCU expects 3 bytes so when issuing the stop command just send "stop.order" and any value for the "sign" and "value" bits, they are not used.
			----------
			stop.order
	                0
		        0
			----------
					"""



	       	####################################

		a=joystick.get_axis(0)	#Steering
		a=int(a*85)

		if a<0 and a>-131:
			servo.sign = 0
			servo.value = -a
		elif a>0 and a<131:
			servo.sign = 1
			servo.value = a*1.3
		else:
			servo.sign = 0
			servo.value = 0

		# print(servo.sign, servo.value)

		pub.publish(servo.order, servo.sign, servo.value)

####################################
		for event in pygame.event.get():
			pass

		f=joystick.get_axis(5)	#Forward	#Drive
		c=joystick.get_axis(2)	#Backward
		f=int((f+1)/2*255)
		c=int((c+1)/2*255)

		if f == 0 and c == 0:
			drive.sign = 0
			drive.value = 0
		elif f != 0 and c == 0 and f<256:
			drive.sign = 0
			drive.value = f
		elif f == 0 and c != 0 and c<256:
			drive.sign = 1
			drive.value = c
		elif f != 0 and c != 0 and f>c and f<256:
			drive.sign = 0
			drive.value = f-c
		elif f != 0 and c != 0 and f<c and c<256:
			drive.sign = 1
			drive.value = c-f
		elif f != 0 and c != 0 and f==c and f<256:
			drive.sign = 0
			drive.value = 0
		else:
			drive.sign = 0
			drive.value = 0

		pub.publish(drive.order, drive.sign, drive.value)

	       	####################################

		b=joystick.get_axis(1)	#Arm motor
		b=int(b*255)

		if b<0 and b>-256:
			motor_arm.sign = 0
			motor_arm.value = -b
		elif b>0 and b<256:
			motor_arm.sign =  1
			motor_arm.value = b
		else:
			motor_arm.sign = 0
			motor_arm.value = 0

		pub.publish(motor_arm.order, motor_arm.sign, motor_arm.value)

	       	####################################

		e=joystick.get_axis(4)	#Bucket motor
		e=int(e*255)

		if e<0 and e>-256:
			motor_bucket.sign = 1
			motor_bucket.value = -e
		elif e>0 and e<256:
			motor_bucket.sign = 0
			motor_bucket.value = e
		else:
			motor_bucket.sign = 0
			motor_bucket.value = 0

		pub.publish(motor_bucket.order, motor_bucket.sign, motor_bucket.value)

	       	####################################

		k=joystick.get_button(1) #B(red)	#Button for stopping everything
		if k == 1:
			pass
			#pub.publish(stop.order, 0, 0)
		else:
			pass

	       	####################################

               	r.sleep()	# Waits a bit to maintain the frequency specified in rospy.Rate()

		####################################


if __name__ == '__main__':

	# Assugb a binary representation (uint8) to each order and initilize the sign and value as 0.
        servo = Order(2, 0, 0)
        drive = Order(3, 0, 0)
        motor_arm = Order(4, 0, 0)
        motor_bucket = Order(5, 0, 0)
        stop = Order(8, 0, 0)

	try:
		talker()
	except rospy.ROSInterruptException:
		pass

pygame.quit()

